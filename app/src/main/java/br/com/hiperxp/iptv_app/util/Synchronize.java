package br.com.hiperxp.iptv_app.util;

import android.content.Context;
import android.content.Intent;
import android.util.Log;

import java.util.List;

import br.com.hiperxp.iptv_app.acivity.MainActivity;
import br.com.hiperxp.iptv_app.dao.MovieDAO;
import br.com.hiperxp.iptv_app.dao.SessionDAO;
import br.com.hiperxp.iptv_app.models.Movie;
import br.com.hiperxp.iptv_app.models.Resultado;
import br.com.hiperxp.iptv_app.models.Session;
import br.com.hiperxp.iptv_app.services.ApiService;
import br.com.hiperxp.iptv_app.services.ServiceConfig;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class Synchronize {

    private Retrofit retrofit;

    private Resultado resultado;
    private List<Movie> movies;

    private Context context;

    public void sync(Context ctxt){
        Session session = new Session();
        session.setUsername("ricardolzp");
        session.setPassword("ricardolzp");
        session.setAuth("true");
        session.setJwt("1234556");

        new SessionDAO(ctxt).salvar(session);
        context = ctxt;
        recuperaDados();
        Intent intent = new Intent(ctxt, MainActivity.class);
        ctxt.startActivity(intent);
    }

    public void recuperaDados() {
        retrofit = ServiceConfig.getRetrofit();

        ApiService apiService = retrofit.create(ApiService.class);

        apiService.getSync().enqueue(new Callback<Resultado>() {
            @Override
            public void onResponse(Call<Resultado> call, Response<Resultado> response) {
                if( response.isSuccessful() ){
                    resultado = response.body();
                    syncMovie(resultado.data.movie);
                }

                Log.d("resultado", "resultado: " + response.toString() );
            }

            @Override
            public void onFailure(Call<Resultado> call, Throwable t) {
                Log.d("resultado", "erro: ");
            }
        });

    }

    public void syncMovie(List<Movie> movies) {

        for(int i=0; i<movies.size(); i++) {
            Movie movie = new Movie();
            Movie movie1 = new Movie();
            movie = movies.get(i);
            new MovieDAO(context).salvar(movie);
        }

    }

}

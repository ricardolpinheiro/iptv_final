package br.com.hiperxp.iptv_app.migrations;

import android.database.sqlite.SQLiteDatabase;
import android.util.Log;


public class CreateSessionTable {

    public static String TABLE_NAME = "session";

    public static void up(SQLiteDatabase db) {
        String sql = "CREATE TABLE IF NOT EXISTS " + TABLE_NAME +
                "(id INTEGER, username VARCHAR, password VARCHAR, auth BOOLEAN, jwt VARCHAR)";
        try {
            db.execSQL(sql);
            Log.i("INFO DB", "TABELAS CRIADAS");
        } catch (Exception e) {
            Log.i("INFO DB", "ERRO NAS CRIAÇAO DE TABELA");
        }
    }

    public static void reload(SQLiteDatabase db) {
        String sql = "DROP TABLE IF EXISTS " + TABLE_NAME + ";";

        try {
            db.execSQL(sql);
            up(db);
            Log.i("INFO DB", "TABELAS CRIADAS");
        } catch (Exception e) {
            Log.i("INFO DB", "ERRO NAS CRIAÇAO DE TABELA");
        }
    }
}

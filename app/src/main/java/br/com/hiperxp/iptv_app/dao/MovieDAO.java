package br.com.hiperxp.iptv_app.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.List;

import br.com.hiperxp.iptv_app.migrations.Database;
import br.com.hiperxp.iptv_app.models.Movie;

public class MovieDAO extends BaseDAO{

    private SQLiteDatabase write;
    private SQLiteDatabase read;

    public MovieDAO(Context ctxt) {
        Database db = new Database(ctxt);
        write = db.getWritableDatabase();
        read = db.getReadableDatabase();
    }

    public List<Movie> get() {
        List<Movie> movies = new ArrayList<>();

        String sql = "SELECT * FROM movies limit 0,50";
        Cursor c = read.rawQuery(sql, null);

        while ( c.moveToNext() ){
            Movie movie = new Movie();
            movie = parseMovie(c);
            movies.add( movie );
        }

        c.close();
        return movies;
    }


    public boolean salvar(Movie movie) {

        ContentValues cv = new ContentValues();

        cv.put("uuid", movie.getUuid());
        cv.put("tmdb_id", movie.getTmdb_id());
        cv.put("title", movie.getTitle());
        cv.put("category_id", movie.getCategory_id());
        cv.put("adult", movie.getAdult());
        cv.put("duration", movie.getDuration());
        cv.put("thumbnail", movie.getThumbnail());
        cv.put("poster", movie.getPoster());
        cv.put("collection_id", movie.getCollection_id());
        cv.put("trailer_url", movie.getTrailer_url());
        cv.put("detail", movie.getDetail());
        cv.put("rating", movie.getRating());
        cv.put("released", movie.getReleased());
        cv.put("type", movie.getType());
        cv.put("year", movie.getYear());

        try {
            write.insert(Database.TABELA_MOVIES, null, cv);
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }

        return true;
    };

}

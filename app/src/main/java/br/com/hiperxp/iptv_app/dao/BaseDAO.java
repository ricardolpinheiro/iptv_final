package br.com.hiperxp.iptv_app.dao;

import android.database.Cursor;

import br.com.hiperxp.iptv_app.models.Movie;
import br.com.hiperxp.iptv_app.models.Session;

public class BaseDAO {

    Session parseSession(Cursor c) {
        Session session = new Session();
        Long id = c.getLong( c.getColumnIndex("id") );
        String sessionUsername = c.getString( c.getColumnIndex("username") );
        String sessionPassword = c.getString( c.getColumnIndex("password") );

        String sessionAuth = c.getString( c.getColumnIndex("auth") );
        String sessionJwt = c.getString( c.getColumnIndex("jwt") );

        session.setId( id );
        session.setUsername( sessionUsername );
        session.setPassword( sessionPassword );
        session.setJwt( sessionAuth );
        session.setAuth( sessionJwt );
        return session;
    }

    Movie parseMovie(Cursor c) {
        Movie movie = new Movie();

        //Long id = c.getLong( c.getColumnIndex("id") );
        String sessionUuid = c.getString(c.getColumnIndex("uuid"));
        String sessionTmdb_id = c.getString(c.getColumnIndex("tmdb_id"));
        String sessionTitle = c.getString(c.getColumnIndex("title"));
        String sessionCategory_id = c.getString(c.getColumnIndex("category_id"));
        String sessionAdult = c.getString(c.getColumnIndex("adult"));
        String sessionDuration = c.getString(c.getColumnIndex("duration"));
        String sessionThumbnail = c.getString(c.getColumnIndex("thumbnail"));
        String sessionPoster = c.getString(c.getColumnIndex("poster"));
        String sessionCollection_id = c.getString(c.getColumnIndex("collection_id"));
        String sessionTrailer_url = c.getString(c.getColumnIndex("trailer_url"));
        String sessionDetail = c.getString(c.getColumnIndex("detail"));
        String sessionRating = c.getString(c.getColumnIndex("rating"));
        String sessionReleased = c.getString(c.getColumnIndex("released"));
        String sessionType = c.getString(c.getColumnIndex("type"));
        String sessionYear = c.getString(c.getColumnIndex("year"));


        movie.setUuid(sessionUuid);
        movie.setTmdb_id(sessionTmdb_id);
        movie.setTitle(sessionTitle);
        movie.setCategory_id(sessionCategory_id);
        movie.setAdult(sessionAdult);
        movie.setDuration(sessionDuration);
        movie.setThumbnail(sessionThumbnail);
        movie.setPoster(sessionPoster);
        movie.setCollection_id(sessionCollection_id);
        movie.setTrailer_url(sessionTrailer_url);
        movie.setDetail(sessionDetail);
        movie.setRating(sessionRating);
        movie.setReleased(sessionReleased);
        movie.setType(sessionType);
        movie.setYear(sessionYear);

        return movie;
    }

}

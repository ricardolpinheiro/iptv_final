package br.com.hiperxp.iptv_app.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import br.com.hiperxp.iptv_app.migrations.Database;
import br.com.hiperxp.iptv_app.models.Session;

public class SessionDAO extends BaseDAO {

    private SQLiteDatabase write;
    private SQLiteDatabase read;

    public SessionDAO(Context ctxt) {
        Database db = new Database(ctxt);
        write = db.getWritableDatabase();
        read = db.getReadableDatabase();
    }

    public List<Session> get() {
        List<Session> sessions = new ArrayList<>();

        String sql = "SELECT * FROM session";
        Cursor c = read.rawQuery(sql, null);

        while ( c.moveToNext() ){
            Session session = new Session();
            session = parseSession(c);
            sessions.add( session );
        }

        c.close();
        return sessions;
    }

    public boolean salvar(Session session) {
        ContentValues cv = new ContentValues();

        cv.put("username", session.getUsername() );
        cv.put("password", session.getPassword() );
        cv.put("jwt", session.getJwt() );
        cv.put("auth", session.getAuth() );

        try {
            write.insert(Database.TABLE_SESSION, null, cv );
            Log.i("INFO", "Tarefa salva com sucesso!");
        }catch (Exception e){
            Log.e("INFO", "Erro ao salvar tarefa " + e.getMessage() );
            return false;
        }

        return true;
    }

}

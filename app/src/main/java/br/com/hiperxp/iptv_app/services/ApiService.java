package br.com.hiperxp.iptv_app.services;


import br.com.hiperxp.iptv_app.models.Resultado;
import retrofit2.Call;
import retrofit2.http.GET;

public interface ApiService {
    @GET("sync")
    Call<Resultado> getSync();
}


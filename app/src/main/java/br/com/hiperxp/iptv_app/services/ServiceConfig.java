package br.com.hiperxp.iptv_app.services;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ServiceConfig {

    private Retrofit retrofit;

    public static Retrofit getRetrofit() {
        return new Retrofit.Builder()
                .baseUrl("http://hiperxp.com.br/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }

}

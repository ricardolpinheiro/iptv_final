package br.com.hiperxp.iptv_app.migrations;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class Database extends SQLiteOpenHelper {
    public static int VERSION = 1;
    public static String NOME_DB = "iptv2";
    public static String TABLE_SESSION = "session";
    public static String TABELA_MOVIES = "movies";


    public Database(Context context) {
        super(context, NOME_DB, null, VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        CreateSessionTable.up(db);
        CreateMovieTable.up(db);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        CreateSessionTable.reload(db);
        CreateMovieTable.reaload(db);

    }
}

package br.com.hiperxp.iptv_app.acivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;


import br.com.hiperxp.iptv_app.R;
import br.com.hiperxp.iptv_app.util.Synchronize;

public class LoginActivity extends BaseActivity {

    private Button btn_login;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        boolean auth = checkAuth();

        if(auth) {
            callMainIntent();
        }

        btn_login = findViewById(R.id.btn_login);

        btn_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new Synchronize().sync(getApplicationContext());
                finish();
            }
        });
    }
}

package br.com.hiperxp.iptv_app.migrations;

import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

public class CreateMovieTable {

    public static void up(SQLiteDatabase db) {
        String sql = "CREATE TABLE IF NOT EXISTS movies (id INTEGER PRIMARY KEY AUTOINCREMENT, " +
                " uuid VARCHAR, tmdb_id VARCHAR, title VARCHAR, category_id VARCHAR, adult VARCHAR, duration VARCHAR, thumbnail VARCHAR, poster VARCHAR, collection_id VARCHAR, trailer_url VARCHAR, detail TEXT, rating VARCHAR, released VARCHAR, type VARCHAR, year VARCHAR ); ";

        try {
            db.execSQL( sql );
        }catch (Exception e){
            Log.i("INFO DB", "Erro ao criar a tabela" + e.getMessage() );
        }
    }

    public static void reaload(SQLiteDatabase db) {
        String sql = "DROP TABLE IF EXISTS movies ;" ;

        try {
            db.execSQL( sql );
            up(db);
        }catch (Exception e){
            Log.i("INFO DB", "Erro ao atualizar App" + e.getMessage() );
        }
    }

}

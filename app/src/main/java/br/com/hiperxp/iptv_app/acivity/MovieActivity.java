package br.com.hiperxp.iptv_app.acivity;

import android.app.ActivityOptions;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.io.Serializable;
import java.util.List;

import br.com.hiperxp.iptv_app.R;
import br.com.hiperxp.iptv_app.adapter.AdapterMovie;
import br.com.hiperxp.iptv_app.dao.MovieDAO;
import br.com.hiperxp.iptv_app.models.Movie;
import br.com.hiperxp.iptv_app.services.ServiceConfig;
import br.com.hiperxp.iptv_app.util.RecyclerItemClickListener;
import retrofit2.Retrofit;

public class MovieActivity extends BaseActivity {

    private AdapterMovie adapterMovie;
    private RecyclerView recyvleMovie;
    private List<Movie> movies;

    //Retrofit
    private Retrofit retrofit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_movie);
        setUpActionBar();
        initView();
    }

    private void initView() {
        recyvleMovie = findViewById(R.id.recyvleMovie);
        retrofit = ServiceConfig.getRetrofit();
        getMovies();
        configuraRecileView();
    }

    private void getMovies() {
        movies = new MovieDAO(getApplicationContext()).get();
    }

    private void configuraRecileView() {
        adapterMovie = new AdapterMovie(movies, this);
        recyvleMovie.setHasFixedSize( true );

        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(this, 6);
        recyvleMovie.setLayoutManager(layoutManager);

        recyvleMovie.addOnItemTouchListener(
                new RecyclerItemClickListener(
                        this,
                        recyvleMovie,
                        new RecyclerItemClickListener.OnItemClickListener() {
                            @Override
                            public void onItemClick(View view, int position) {
                                Movie m = movies.get(position);
                                Intent i = new Intent(MovieActivity.this, DetailActivity.class);

                                i.putExtra("movie", (Serializable) m);
                                i.putExtra("type", "movie");

                                ActivityOptions options = ActivityOptions.makeSceneTransitionAnimation(MovieActivity.this,
                                        view.findViewById(R.id.poster),"sharedName");

                                startActivity(i,options.toBundle());
                            }

                            @Override
                            public void onLongItemClick(View view, int position) {

                            }

                            @Override
                            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                            }
                        }
                )
        );

        recyvleMovie.setAdapter( adapterMovie );
    }
}

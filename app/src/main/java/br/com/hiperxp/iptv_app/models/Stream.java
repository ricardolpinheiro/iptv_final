package br.com.hiperxp.iptv_app.models;

public class Stream {
    private int id;
    private String name;
    private String category_id;
    private String stream_logo;
    private String epg_id;
    private String host_id;
    private String stream_link;
    private String notes;
    private int status;
    private String deleted_by;
    private String deleted_at;
    private String created_at;
    private String updated_at;

    public Stream(int id, String name, String category_id, String stream_logo, String epg_id, String host_id, String stream_link, String notes, int status, String deleted_by, String deleted_at, String created_at, String updated_at) {
        this.id = id;
        this.name = name;
        this.category_id = category_id;
        this.stream_logo = stream_logo;
        this.epg_id = epg_id;
        this.host_id = host_id;
        this.stream_link = stream_link;
        this.notes = notes;
        this.status = status;
        this.deleted_by = deleted_by;
        this.deleted_at = deleted_at;
        this.created_at = created_at;
        this.updated_at = updated_at;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCategory_id() {
        return category_id;
    }

    public void setCategory_id(String category_id) {
        this.category_id = category_id;
    }

    public String getStream_logo() {
        return stream_logo;
    }

    public void setStream_logo(String stream_logo) {
        this.stream_logo = stream_logo;
    }

    public String getEpg_id() {
        return epg_id;
    }

    public void setEpg_id(String epg_id) {
        this.epg_id = epg_id;
    }

    public String getHost_id() {
        return host_id;
    }

    public void setHost_id(String host_id) {
        this.host_id = host_id;
    }

    public String getStream_link() {
        return stream_link;
    }

    public void setStream_link(String stream_link) {
        this.stream_link = stream_link;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getDeleted_by() {
        return deleted_by;
    }

    public void setDeleted_by(String deleted_by) {
        this.deleted_by = deleted_by;
    }

    public String getDeleted_at() {
        return deleted_at;
    }

    public void setDeleted_at(String deleted_at) {
        this.deleted_at = deleted_at;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }
}

package br.com.hiperxp.iptv_app.acivity;

import android.content.Context;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.os.Bundle;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.material.chip.Chip;
import com.google.android.material.chip.ChipGroup;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.navigation.NavigationView;
import com.squareup.picasso.Picasso;

import java.util.zip.Inflater;

import br.com.hiperxp.iptv_app.R;
import br.com.hiperxp.iptv_app.models.Movie;
import br.com.hiperxp.iptv_app.models.Movie;
import br.com.hiperxp.iptv_app.util.UiUtils;

public class DetailActivity extends BaseActivity {


    private TextView title;
    private TextView description;
    private ImageView poster;
    private ImageView background;
    private FloatingActionButton play_fab;
    private NavigationView navigationView;
    private ChipGroup chipGroup;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        initView();
    }

    public void initView() {
        Bundle dados = getIntent().getExtras();
        Movie movie = (Movie) dados.getSerializable("movie");

        chipGroup = findViewById(R.id.chip_group);
        Context context = chipGroup.getContext();

        Chip chip = new Chip(context);
        chip.setText("teste");
        chip.setChipStrokeWidth(UiUtils.dipToPixels(context, 1));
        chip.setChipStrokeColor(ColorStateList.valueOf(
                context.getResources().getColor(R.color.white)));
        chipGroup.addView(chip);

        chip.setText("teste");
        chip.setChipStrokeWidth(UiUtils.dipToPixels(context, 1));
        chip.setChipStrokeColor(ColorStateList.valueOf(
                context.getResources().getColor(R.color.white)));
        chipGroup.addView(chip);

//        play_fab = findViewById(R.id.play_fab);
//        title = findViewById(R.id.detail_movie_title);
//        description = findViewById(R.id.detail_movie_desc);
//        poster = findViewById(R.id.detail_movie);
//        background = findViewById(R.id.background_movie);
//
//        // setup animation
//        play_fab.setAnimation(AnimationUtils.loadAnimation(this,R.anim.scale_animation));
//        poster.setAnimation(AnimationUtils.loadAnimation(this,R.anim.scale_animation));
//
//        play_fab.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//            }
//        });

        movieDetail(movie);
    }

    private void movieDetail(Movie movie) {


//        title.setText(movie.getTitle());
//        description.setText(movie.getDetail());
//
//        if(!movie.getPoster().isEmpty())
//            Picasso.get().load(movie.getPoster()).into(poster);
//
//        if(!movie.getThumbnail().isEmpty())
//            Picasso.get().load(movie.getThumbnail()).into(background);

    }
}
